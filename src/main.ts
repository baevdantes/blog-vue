import Vue from "vue";
import app from "./app.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import "bootstrap-4-grid/css/grid.min.css";
import "./main.scss";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(app),
}).$mount("#app");
