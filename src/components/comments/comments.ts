import { Component, Prop, Vue } from "vue-property-decorator";
import { IPost } from "@/store/interfaces/IAppState";
import btn from "@/components/btn.vue";
import { isNotEmpty } from "@/utils";

export interface ICommentForm {
  name: string;
  text: string;
  postId: number;
}

@Component({
  components: {
    btn,
  },
})
export default class Comments extends Vue {
  @Prop() post: IPost;

  form: ICommentForm = {
    name: "",
    text: "",
    postId: this.post.id,
  };

  get isValidForm(): boolean {
    return !!(
      this.form.name.length &&
      this.form.text.length &&
      isNotEmpty(this.form.name) &&
      isNotEmpty(this.form.text)
    );
  }

  addComment() {
    this.$emit("addComment", this.form);
    this.form = {
      text: "",
      name: "",
      postId: this.post.id,
    };
  }
}
