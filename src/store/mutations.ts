import { MutationTree } from "vuex";
import IAppState, { IPost } from "@/store/interfaces/IAppState";
import { ICommentForm } from "@/components/comments/comments";

export const setPosts = "setPosts";
export const setPost = "setPost";
export const setComment = "setComment";
export const refreshPosts = "refreshPosts";

const mutations: MutationTree<IAppState> = {
  [setPosts]: (state: IAppState, posts: IPost[]) => {
    state.posts = posts;
  },

  [setPost]: (state: IAppState, post: IPost) => {
    state.currentPost = post;
  },

  [refreshPosts]: (state: IAppState, payload: IPost[]) => {
    state.posts = payload;
  },

  [setComment]: (state: IAppState, comment: ICommentForm) => {
    state.posts.find((post: IPost) => post.id === comment.postId).comments.unshift({
      name: comment.name,
      text: comment.text,
    });
  },
};

export default mutations;
