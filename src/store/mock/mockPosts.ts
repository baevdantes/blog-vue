import { IPost } from "@/store/interfaces/IAppState";

const mockPosts: IPost[] = [
  {
    text: "text",
    id: 1,
    title: "title",
    comments: [],
  },
];

export default mockPosts;
