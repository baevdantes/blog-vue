import { Component, Vue } from "vue-property-decorator";
import card from "@/components/card.vue";
import btn from "@/components/btn.vue";
import { State } from "vuex-class";
import { IPost } from "@/store/interfaces/IAppState";

@Component({
  components: {
    card,
    btn,
  },
})
export default class Home extends Vue {
  @State("posts") posts: IPost[];
  postsNumber: number = 4;
  step: number = 4;

  get slicedPosts(): IPost[] {
    return this.posts.slice(0, this.postsNumber);
  }

  get isVisibleLoadMoreBtn(): boolean {
    return (this.slicedPosts.length < this.posts.length);
  }

  loadMore() {
    this.postsNumber = this.postsNumber + this.step;
  }

  goToAddPost() {
    this.$router.push("addPost").then();
  }
}
