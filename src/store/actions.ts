import { ActionTree } from "vuex";
import IAppState, { IPost } from "@/store/interfaces/IAppState";
import { refreshPosts, setComment, setPost, setPosts } from "@/store/mutations";
import { IAddPostForm } from "@/views/add-post/add-post";
import router from "@/router";
import { ICommentForm } from "@/components/comments/comments";

export const getPosts = "getPosts";
export const getPost = "getPost";
export const addPost = "addPost";
export const initHomePage = "initHomePage";
export const initPostPage = "initPostPage";
export const storagePosts = "posts";
export const addComment = "addComment";

const actions: ActionTree<IAppState, IAppState> = {
  [getPosts]: ({ commit }) => {
    if (JSON.parse(localStorage.getItem("posts"))) {
      commit(setPosts, JSON.parse(localStorage.getItem("posts")));
    } else {
      commit(setPosts, []);
    }
  },

  [getPost]: ({ commit, dispatch, state }, payload: number) => {
    dispatch(getPosts).then(() => {
      const foundPost: IPost = state.posts.find((post: IPost) => post.id === payload);
      if (foundPost) {
        commit(setPost, foundPost);
      }
    });
  },

  [addPost]: ({ commit, state }, payload: IAddPostForm) => {
    if (!localStorage.getItem(storagePosts)) {
      localStorage.setItem(storagePosts, JSON.stringify([
        { ...payload, id: 1, comments: [] },
      ]));
    } else {
      const data: IPost[] = JSON.parse(localStorage.getItem("posts"));
      data.unshift({ ...payload, id: ++state.posts.length, comments: [] });
      localStorage.setItem(storagePosts, JSON.stringify(data));
    }
    commit(refreshPosts, JSON.parse(localStorage.getItem("posts")));
    router.push(`/post/${state.posts.length}`).then();
  },

  [addComment]: ({ commit }, comment: ICommentForm) => {
    const data: IPost[] = JSON.parse(localStorage.getItem("posts"));
    data.find((post: IPost) => post.id === comment.postId).comments.unshift({
      text: comment.text,
      name: comment.name,
    });
    localStorage.setItem("posts", JSON.stringify(data));
    commit(setComment, comment);
  },

  [initHomePage]: ({ dispatch }) => {
    dispatch(getPosts).then();
  },

  [initPostPage]: ({ dispatch }, payload: number) => {
    dispatch(getPost, payload).then();
  },
};

export default actions;
