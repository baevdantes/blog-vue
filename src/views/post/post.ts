import { Component, Vue } from "vue-property-decorator";
import { Action, State } from "vuex-class";
import { IPost } from "@/store/interfaces/IAppState";
import Comments from "@/components/comments/comments.vue";
import { addComment } from "@/store/actions";
import { ICommentForm } from "@/components/comments/comments";

@Component({
  components: {
    Comments,
  },
})
export default class Post extends Vue {
  @State("currentPost") post: IPost;
  @Action(addComment) addComment: (comment: ICommentForm) => void;
}
