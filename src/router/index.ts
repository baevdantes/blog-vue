import Vue from "vue";
import VueRouter, { Route, RouteConfig } from "vue-router";
import store from "@/store";
import { initHomePage, initPostPage } from "@/store/actions";

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: "/",
    name: "home",
    component: () => import( "@/views/home/home.vue"),
    beforeEnter: (to: Route, from: Route, next: () => void) => {
      store.dispatch(initHomePage).then(() => next());
    },
  },
  {
    path: "/post/:id",
    name: "post",
    component: () => import("@/views/post/post.vue"),
    beforeEnter: (to: Route, from: Route, next: () => void) => {
      store.dispatch(initPostPage, Number(to.params.id)).then(() => next());
    },
  },
  {
    path: "/addPost",
    name: "addPost",
    component: () => import("@/views/add-post/add-post.vue"),
  },
  {
    path: "*",
    name: "404",
    component: () => import("@/views/404.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
