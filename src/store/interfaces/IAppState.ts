export default interface IAppState {
  posts: IPost[];
  currentPost: IPost;
}

export interface IPost {
  id: number;
  title: string;
  text: string;
  comments: IComment[];
}

export interface IComment {
  name: string;
  text: string;
}
