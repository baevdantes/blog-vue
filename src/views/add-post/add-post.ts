import { Component, Vue } from "vue-property-decorator";
import btn from "@/components/btn.vue";
import { Action } from "vuex-class";
import { isNotEmpty } from "@/utils";

export interface IAddPostForm {
  title: string;
  text: string;
}

@Component({
  components: {
    btn,
  },
})
export default class AddPost extends Vue {
  form: IAddPostForm = {
    text: "",
    title: "",
  };

  @Action("addPost") addPost: (value: IAddPostForm) => void;

  get isValidForm(): boolean {
    return !!(
      this.form.text.length &&
      this.form.title.length &&
      isNotEmpty(this.form.title) &&
      isNotEmpty(this.form.text)
    );
  }
}
