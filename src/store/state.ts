import IAppState from "@/store/interfaces/IAppState";

const state: IAppState = {
  posts: [],
  currentPost: null,
};

export default state;
